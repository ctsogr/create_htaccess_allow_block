#!/bin/bash
#
#
# This script generates a new .htaccess file and deny's any
#ip other than those included in *.inc_ips eg:
# uk_country_allow.inc_ips
# monitoring_tools.inc_ips
# .... etc
#
# Must be present the file original_htaccess. The denys will take REPLACE
#of the line: #ips_to_allow
#
#
# Usefull when you want to deny (or allow) acceess to all but allow (or deny) a specific set
#eg. counntry's ip's , monitoring ip's etc.
#
#
#
script_ver="2022100501"
orig_htaccess="listip-htaccess-template.txt"
dest_htaccess="ok.htaccess"
tmp_file="deleteme.tmp"
now=$(date +"%D %T")
action2take="Require ip"
IPCALC="$(command -v ipcalc)"
# If extended_desc is true then a comment will be added for each ip's source file ( true /false value)
extended_desc=false


## End of settings. Do not change bellow this line #####################
########################################################################
echo "Create htaccess Copyright (C) by Christos Tsompanoglou 2016-2018  ver. $script_ver "
echo "Current time: $(date)"
echo " "
command -v ipcalc >/dev/null 2>&1 || { echo >&2 "I require ipcalc but it's not installed.  Aborting."; exit 1; }
command -v curl >/dev/null 2>&1 || { echo >&2 "I require curl but it's not installed.  Aborting."; exit 1; }


#Google ips etc...
# https://github.com/lord-alfred/ipranges
https://raw.githubusercontent.com/lord-alfred/ipranges/main/google/ipv4_merged.txt




#Download GoogleBot etc IP's list...
echo "Downloading Pingdom IPs..."
curl -L --output google_ipv4_merged.inc_ips.tmp https://my.pingdom.com/probes/ipv4
        if [ $? -ne 0 ]
          then
                echo "Downloading of google/bots FAILED."
                echo "Skiping integration of NEW google/bots list."
                rm -f google_ipv4_merged.inc_ips.tmp
          else
                mv google_ipv4_merged.inc_ips.tmp google_ipv4_merged.inc_ips
        fi


#Download Pingdom IP's list...
echo "Downloading Pingdom IPs..."
curl -L --output pingdom_ips.inc_ips.tmp https://my.pingdom.com/probes/ipv4
        if [ $? -ne 0 ]
          then
                echo "Downloading of Pingdom FAILED."
                echo "Skiping integration of NEW Pingdom list."
                rm -f pingdom_ips.inc_ips.tmp
          else
                mv pingdom_ips.inc_ips.tmp pingdom_ips.inc_ips
        fi


#Download StatusCake IP's list...
echo "Downloading StatusCake IPs..."
curl -L --output statuscake_ips.inc_ips.tmp https://www.statuscake.com/API/Locations/txt
        if [ $? -ne 0 ]
          then
                echo "Downloading of StatusCake FAILED."
                echo "Skiping integration of NEW StatusCake list."
                rm -f statuscake_ips.inc_ips.tmp
          else
                mv statuscake_ips.inc_ips.tmp statuscake_ips.inc_ips
        fi



#Download GR IP's list...
echo "Downloading GR IPs..."
curl -L --output gr_ips.inc_ips.tmp http://www.iwik.org/ipcountry/GR.cidr
        if [ $? -ne 0 ]
          then
                echo "Downloading of GR FAILED."
                echo "Skiping integration of NEW GR list."
                rm -f gr_ips.inc_ips.tmp
          else
                mv gr_ips.inc_ips.tmp gr_ips.inc_ips
        fi





#
#
# START PARSING NOW...
#
#
echo "Start parsing/creating htaccess..."
echo "#"> $tmp_file
echo "#">> $tmp_file
echo "#  This .htaccess file is automatically generated. Changes WILL BE OVERWRITTEN">> $tmp_file
echo "# to edit the .htaccess file change the content of the $orig_htaccess">> $tmp_file
echo "#">> $tmp_file
echo "#">> $tmp_file
echo "# Generated at $now" >> $tmp_file
echo "# Bash shell created Copyright (C) by Christos Tsompanoglou 2016-2018 ver.$script_ver">> $tmp_file
k=1
valid_ip_no=0
while read -r line
	do
        if [[ $line == *"#ips_to_allow"* ]]; then
           #Let's put the  Ip's inside...
           files=$( ls *.inc_ips )
           counter=0
           for ff in $files ; do
             echo Next: $ff
             let counter=$counter+1
             # Parse above file...
             echo "Parsing file no $counter named: $ff"
             echo "########################################"
             if  [ "$extended_desc" = false ];  then
                echo "# #### START # IPs from file $ff ########################" >> $tmp_file
             fi
                k=1
                valid_ip_no=0
                while read -r line2; do
                               if ! [[ $line2 = \#* ]] ; then
                                 if [ ${#line2} -ge 7 ]; then
                                        clearstr="${line2%#*}"
                                        if ($IPCALC -cs $clearstr); then
                                                echo "  $action2take $line2" >> $tmp_file
                                                if [ "$extended_desc" = true ]; then
                                                        echo "#   $line2 loaded from  $ff" >> $tmp_file
                                                fi
                                                ((valid_ip_no++))
                                        else
                                                echo "Invalid ip - ignored: $line2"
                                        fi
                                 fi
                               fi
                             ((k++))
                     done < $ff
                     echo "Total number of lines in file...: $k"
                     echo "Total VALID IPs of lines in file: $valid_ip_no"
                     echo "########################################"
                     if [ "$extended_desc" = false ]; then
                        echo "# #### END # IPs from file $ff ########################" >> $tmp_file
                     fi
                done
        else
          #echo "Line # $k: $line"
          echo "$line" >> $tmp_file
        fi
        ((k++))
	done < $orig_htaccess

cp -v -f $tmp_file $dest_htaccess && rm -f $tmp_file
